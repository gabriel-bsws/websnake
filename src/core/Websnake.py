###########################################################################################################
##                                                                                                       ##
##                        WEBSNAKE - PYTHON MEDIA TOOL FOR WEB DEVELOPMENT                               ##
##                                                                                                       ##
##          Write PHP, do PYTHON! Have you ever tried to use PHP to process media issues, as image       ##
##          rendering, for example? It is painful and the result is almost always less than expected.    ##
##          Websnake Media Tool, was created to fulfill this deficit. It's developed in PYTHON, which is ##
##          much more indicated to process media, but the usage is entirely through an easy              ##
##          PHP interface, which requires just a basic knowledge in PHP and Object Oriented Programming. ##
##          Enjoy using and make powerful web applications with basic web development skills.            ##
##                                                                                                       ##
##                                                                                                       ##
##          Websnake Python Media Tool - Copyright (c) 2014 BSWS I.T. Solutions                          ##
##                                                                                                       ##
##          >>> CONTACT DEVELOPMENT TEAM:                                                                ##
##          >>> BSWS I.T. Solutions                                                                      ##
##          >>> Website: http://bsws.net.br                                                              ##
##          >>> Email: contato@bsws.net.br                                                               ##
##          >>> Skype: gabriel-bsws                                                                      ##
##          >>> Team members: Gabriel Guelfi, Thiago Guelfi                                              ##
##                                                                                                       ##
##                                                                                                       ##
##          This file is part of Websnake Python Media Tool.                                             ##
##                                                                                                       ##
##          Websnake Python Media Tool is free software: you can redistribute it and/or modify           ##
##          it under the terms of the GNU General Public License as published by                         ##
##          the Free Software Foundation, either version 3 of the License.                               ##
##                                                                                                       ##
##          Websnake Python Media Tool is distributed in the hope that it will be useful,                ##
##          but WITHOUT ANY WARRANTY; without even the implied warranty of                               ##
##          MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                                ##
##          GNU General Public License for more details.                                                 ##
##                                                                                                       ##
##          You should have received a copy of the GNU General Public License                            ##
##          along with this copy of Websnake Python Media Tool under the name of LICENSE.pdf.            ##
##          If not, see <http://www.gnu.org/licenses/>.                                                  ##                                            
##                                                                                                       ##
##          Using, modifying and/or running this software or any of its contents, implies consent        ##
##          to the terms and conditions explicit within the license, mentioned above.                    ##
##                                                                                                       ##
###########################################################################################################


# Importing built-in classes:
import sys
import os
import uuid
import json
import inspect
import math

# >>> WEBSNAKE PYTHON MAIN CLASS <<<
#
# This class has the required methods and properties for the tool.
#
# Declares global properties and methods, which are accessible from any place inside the python engine.
#
class Websnake:
    # >>>> GLOBAL PROPERTIES:
    
    # Built-in "sys" module global instance:
    sys = sys
    # Built-in "math" module global instance:
    math = math
    # Built-in "os" module global instance:
    os = os
    # Absolute path to the Websnake's root directory:
    root = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..'))
    # Global message:
    see_doc_msg = "See documentation at \"http://labs.bsws.net.br/websnakeproject\" for further information."
    # The relation of php datatypes and its names within the Websnake API:
    php_datatypes = {
        'int':"integer",
        'unicode':"string",
        'str':"string",
        'bool':"boolean",
        'list':"array",
        'dict':"associative array",
        'float':'float'
    }
    # >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    
    # >>>> GLOBAL METHOD require_args:
    #
    # This method is called as the first statement within module's methods which requires arguments.
    #Verifies if exists any argument setted. If not, throw FATAL error.
    #
    # For arguments with specific name and type, you can pass the list of these specific arguments and
    #its related types. The method "require_args" will check if the arguments with the specific name exists
    #and if its types are correct.
    #
    # Throw FATAL Errors if any of these requirements are not met.
    #
    def require_args(self, _argnames = 0, _argtypes = 0):
        f_obj = inspect.stack()[1][0]
        methodname = f_obj.f_code.co_name
        classname = inspect.getmodule(f_obj).__name__
        
        if not self.params:
            print " FATAL: Websnake@ParamError: Method \""+methodname+"()\" from class \""+classname+"\" requires arguments. No argument found. "+self.see_doc_msg+"\n"
            sys.exit()
        
        if type(_argnames) is list and type(_argtypes) is list:
            if not len(_argnames) == len(_argtypes):
                print "FATAL: Websnake@InternalError: In \"core.Websnake.require_args()\" from \""+classname+"."+methodname+"()\": \"_argnames\" and \"_argtypes\" lengths must match each other."+"\n"
                sys.exit()
            else:
                error = 0
                i = 0
                for key in _argnames:
                    if not self.params.has_key(key):
                        print " FATAL: Websnake@ParamError: Method \""+methodname+"()\" from class \""+classname+"\" requires "+self.php_datatypes[_argtypes[i]]+" type argument named \""+key+"\" which was not found. "+self.see_doc_msg+"\n"
                        error = 1
                    elif not type(self.params[key]).__name__ == _argtypes[i]:
                        print " FATAL: Websnake@TypeError: Method \""+methodname+"()\" from class \""+classname+"\": Type of argument \""+key+"\" must be "+self.php_datatypes[_argtypes[i]]+". "+self.php_datatypes[type(self.params[key]).__name__].capitalize()+" found, instead. "+self.see_doc_msg+"\n"
                        error = 1
                        
                    i = i + 1
                i = 0
                
                if error:
                    sys.exit()
    # >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    
    # >>>> GLOBAL METHOD set_params:
    #
    # THIS METHOD IS A GLOBAL REQUIREMENT. It's called as the first statement in all "__init__" methods within the modules.
    #
    # It decodes all the data which enters the python engine and set it to a global propertie object named "params".
    #OBS:If the data is too large, it will be inside an input file, so this method load and decode the file
    #before make the proper settings.
    def set_params(self, _params):
        params = self.decode_json(_params)
        if params.has_key('__largeinput__'):
            inputfile = open(params['__largeinput__'],"r")
            inputdata = json.load(inputfile)
            inputfile.close()
            if not bool(params['debug']):
                os.remove(params['__largeinput__'])
            params = inputdata
            
        self.params = (0 if not _params else params)
        
    # >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    
    # >>>> GLOBAL METHOD uniqid:
    #
    #  Returns an unique ID string.
    def uniqid(self):
        uid = str(uuid.uuid1())
        return uid.replace("-", "")
    # >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    
    # >>>> GLOBAL METHOD encode_json:
    #
    # Receives data and returns a encoded JOSN object.
    def encode_json(self, _params):
        return json.dumps(_params)
    # >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    
    # >>>> GLOBAL METHOD decode_json:
    #
    # Receive string formated as JSON and returns a dictionary with the data within that:
    def decode_json(self, _params):
        return json.loads(json.dumps(_params))